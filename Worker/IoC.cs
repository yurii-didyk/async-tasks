﻿using System;
using Worker.Services;
using Worker.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjectStructureWebAPI.Interfaces;
using ProjectStructureWebAPI.QueueServices;
using RabbitMQ.Client;
using ProjectStructureWebAPI;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using Microsoft.AspNetCore.SignalR.Client;

namespace Worker
{
    class IoC
    {
        public IoC()
        {
            var config = new AppConfiguration();
            var services = new ServiceCollection();
            services.AddScoped<QueueService>();
            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddScoped<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();
            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddScoped<IMessageProducerScopeFactory, MessageProducerScopeFactory>();
            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddScoped<ILogger, Logger>();
            services.AddSingleton<RabbitMQ.Client.IConnectionFactory>(x => new ProjectStructureWebAPI.Models.ConnectionFactory(new Uri(config.AMQP)));
            var serviceProvider = services.BuildServiceProvider();
            var service = serviceProvider.GetService<QueueService>();
            service.Run();
        }
    }
}
