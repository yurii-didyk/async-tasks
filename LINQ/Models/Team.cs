﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LINQ.Models
{
    class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        public override string ToString()
        {
            return new string($"TeamId: {Id}\nTeam name: {Name}\nCreatedAt: {CreatedAt}");
        }
    }
}
