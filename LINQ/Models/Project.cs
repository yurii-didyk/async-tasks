﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LINQ.Models
{
    class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }
        //public IEnumerable<Task> Tasks { get; set; }
        //public User Author { get; set; }
        //public Team Team { get; set; }
        public override string ToString()
        {
            return new string($"ProjectId: {Id}\nProject name: {Name}\nDescription: {Description}\nCreated at: {CreatedAt}\nDeadline: {Deadline}\nAuthorId: {AuthorId}\nTeamId: {TeamId}");
        }
    }
}
