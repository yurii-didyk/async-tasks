﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace LINQ.Models
{
    class TaskCountForProject
    {
        [JsonProperty("project")]
        public Project Project { get; set; }
        [JsonProperty("tasksCount")]
        public int TasksCount { get; set; }

        public override string ToString()
        {
            return $"Project: {Project.ToString()}\nTasksCount: {TasksCount}";
        }
    }
}
