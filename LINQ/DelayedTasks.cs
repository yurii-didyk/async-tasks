﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ProjectStructureWebAPI;
using System.Timers;
using LINQ.Models;
using System.Net.Http;

namespace LINQ
{
    class DelayedTasks
    {
        private readonly Queries queries = new Queries();
        private readonly AppConfiguration config = new AppConfiguration();
        private readonly string URL;

        public DelayedTasks()
        {
            URL = config.Server + "api";
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay = 1000)
        {
            Timer timer = new Timer()
            {
                Interval = delay,
                AutoReset = false,

            };
            var tcs = new TaskCompletionSource<int>();
            timer.Elapsed += async (obj, ev) =>
            {
                try
                {
                    var tasks = await Queries.GetUnfinishedTasks();
                    if (tasks.Count == 0)
                    {
                        Console.WriteLine("All tasks marked as finished");
                        tcs.SetException(new IndexOutOfRangeException());
                    }
                    var taskIndex = new Random().Next(1, tasks.Count);
                    var taskToUpdate = tasks[taskIndex];
                    taskToUpdate.State = TaskStates.Finished;
                    await HelperHttpMethods.PutHttpResponse($"{URL}/tasks/{taskToUpdate.Id}", taskToUpdate);
                    tcs.SetResult(taskToUpdate.Id);
                }
                catch (Exception exception)
                {
                    tcs.SetException(exception);
                }
            };
            timer.Enabled = true;
            return await tcs.Task;
        }


        public async System.Threading.Tasks.Task RandomizeTasksStates()
        {
            var tasks = await Queries.GetAllTasks();
            tasks.ForEach(t => t.State = (TaskStates)new Random().Next(0, 4));
            foreach (var t in tasks)
            {
                await HelperHttpMethods.PutHttpResponse($"{URL}/tasks/{ new Random().Next(1, tasks.Count + 1)}", t);
            }
        }

    }
}
