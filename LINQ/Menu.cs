﻿using System;
using System.Collections.Generic;
using System.Text;
using LINQ.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using ProjectStructureWebAPI;
using System.Threading.Tasks;

namespace LINQ
{
    class Menu
    {
        AppConfiguration config = new AppConfiguration();
        private static readonly string _menu = "1.Get tasks for user`s project(by id)\n2.Get tasks for user, where task name shorter 45 symbols\n3.Get list(id, name) of finished tasks in 2019 for user\n4.Get list(id, team name, list of users) where team members older than 12 y.o\n5.Get list of users sorted by first name and tasks sorted by name length\n6.Get structure 1\n7.Get structure 2\n8.Get logs\n9.Start marking tasks\n10.Randomize tasks states";

        public async System.Threading.Tasks.Task Run()
        {
            //Console.WriteLine("Do you want to load data from BSA2019 API to new custom API?(y/n)");
            //char ch;
            //ch = Convert.ToChar(Console.ReadLine());
            //if (ch == 'y' || ch == 'Y')
            //{
            //    await LoadInfoToAPI();
            //}
            Queries queries = new Queries();
            DelayedTasks delayedTasks = new DelayedTasks();
            while (true)
            {
                Console.Clear();
                Console.WriteLine(_menu);
                short choice;
                Int16.TryParse(Console.ReadLine(), out choice);
                try
                {


                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Input user Id: ");
                            int input;
                            Int32.TryParse(Console.ReadLine(), out input);
                            var query = await Queries.TasksCountForProject(input);
                            if (query == null)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            foreach(var item in query)
                            {
                                Console.WriteLine(item.ToString());
                            }
                            break;
                        case 2:
                            Console.WriteLine("Input user Id: ");
                            int inputId;
                            Int32.TryParse(Console.ReadLine(), out inputId);
                            var query2 = await Queries.TasksForPerformer(inputId);
                            if (query2.Count == 0)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            foreach (Models.Task item in query2)
                            {
                                Console.WriteLine(item.ToString());
                                Console.WriteLine();
                            }
                            break;
                        case 3:
                            Console.WriteLine("Input user Id: ");
                            int inputUserId;
                            Int32.TryParse(Console.ReadLine(), out inputUserId);
                            var query3 = await Queries.FinishedTasksInThisYear(inputUserId);
                            if (query3.Count == 0)
                            {
                                Console.WriteLine("Nothing found!");
                                Console.ReadKey();
                                break;
                            }
                            foreach (var item in query3)
                            {
                                Console.WriteLine($"Id: {item.Item1}\nName: {item.Item2}");
                                Console.WriteLine();
                            }
                            break;
                        case 4:
                            var query4 = await Queries.TeamsOlder12();
                            if (query4.Count == 0)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            foreach (var item in query4)
                            {
                                Console.WriteLine($"TeamId: {item.Item1}\nTeam name:{item.Item2};\n Users: ");
                                foreach (var nest in item.Item3)
                                {
                                    Console.Write($"{nest.ToString()}\n\n");
                                }
                                Console.WriteLine();
                            }
                            break;
                        case 5:
                            var query5 = await Queries.GetSortedUsers();
                            if (query5.Count == 0)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            foreach (var item in query5)
                            {
                                Console.WriteLine(item.Item1.ToString());
                                foreach (var nest in item.Item2)
                                {
                                    Console.WriteLine(nest.ToString());
                                    Console.WriteLine();
                                }
                                Console.WriteLine();
                            }
                            break;
                        case 6:
                            Console.WriteLine("Input user Id: ");
                            int Id;
                            Int32.TryParse(Console.ReadLine(), out Id);
                            var query6 = await Queries.GetModel1(Id);
                            if (query6 == null)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            Console.WriteLine(query6.ToString());
                            break;
                        case 7:
                            Console.WriteLine("Input project Id: ");
                            int ProjectId;
                            Int32.TryParse(Console.ReadLine(), out ProjectId);
                            var query7 = await Queries.GetModel2(ProjectId);
                            if (query7 == null)
                            {
                                Console.WriteLine("Nothing found!");
                                break;
                            }
                            Console.WriteLine(query7.ToString());
                            break;
                        case 8:
                            await HelperHttpMethods.GetHttpResponse(config.Server + "api/logs/request");
                            break;
                        case 9:
                            var id = await delayedTasks.MarkRandomTaskWithDelay();
                            Console.WriteLine($"Task №{id} marked as finished");
                            break;
                        case 10:
                            await delayedTasks.RandomizeTasksStates();
                            break;
                        default:
                            
                            Console.WriteLine("Please, input correct number");
                            break;
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Something goes wrong! Try again...");
                    Console.ReadKey();
                }
                Console.ReadKey();
            }
        }
        private async System.Threading.Tasks.Task LoadInfoToAPI()
        {
            var URL = config.BSA;
            var URL_custom = config.Server + "api";

            List<Project> Projects = JsonConvert.DeserializeObject<List<Project>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/projects")).Content.ReadAsStringAsync());
            foreach (var item in Projects)
            {
                await HelperHttpMethods.PostHttpResponse($"{URL_custom}/projects", item);
            }
            List<Models.Task> Tasks = JsonConvert.DeserializeObject<List<Models.Task>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/tasks")).Content.ReadAsStringAsync());
            foreach (var item in Tasks)
            {
                await HelperHttpMethods.PostHttpResponse($"{URL_custom}/tasks", item);
            }
            List<Team> Teams = JsonConvert.DeserializeObject<List<Team>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/teams")).Content.ReadAsStringAsync());
            foreach (var item in Teams)
            {
                await HelperHttpMethods.PostHttpResponse($"{URL_custom}/teams", item);
            }
            List<User> Users = JsonConvert.DeserializeObject<List<User>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/users")).Content.ReadAsStringAsync());
            foreach (var item in Users)
            {
                await HelperHttpMethods.PostHttpResponse($"{URL_custom}/users", item);
            }
        }
    }
}
