﻿using System;
using System.Collections.Generic;
using System.Text;
using LINQ.Models;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace LINQ
{
    static class HelperHttpMethods
    {
        public static async Task<HttpResponseMessage> GetHttpResponse(string url)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = await client.GetAsync(url);
            }
            return response;
        }
        public static async Task<HttpResponseMessage> PostHttpResponse(string url, object obj)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = await client.PostAsJsonAsync(url, obj);
            }
            return response;
        }

        public static async Task<HttpResponseMessage> PutHttpResponse(string url, object obj)
        {
            HttpResponseMessage response;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                response = await client.PutAsJsonAsync(url, obj);
            }
            return response;
        }
    }
}
