﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net.Http;
using LINQ.Models;
using ProjectStructureWebAPI;
using System.Threading.Tasks;
using System.Timers;

namespace LINQ
{
    class Queries
    {
        private static readonly AppConfiguration config = new AppConfiguration();
        private static string URL = config.Server + "api";

        public static async Task<List<Project>> GetAllProjects()
        {
            var result = JsonConvert.DeserializeObject<List<Project>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/projects")).Content.ReadAsStringAsync());
            return result;
        }

        public static async Task<List<Models.Task>> GetAllTasks()
        {
            var result = JsonConvert.DeserializeObject<List<Models.Task>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/tasks")).Content.ReadAsStringAsync());
            return result;
        }
        public static async Task<List<Models.Task>> GetUnfinishedTasks()
        {
            var result = JsonConvert.DeserializeObject<List<Models.Task>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/tasks")).Content.ReadAsStringAsync());
            return result.Where(t => t.State != TaskStates.Finished).ToList();
        }

        public static async Task<List<Team>> GetAllTeams()
        {
            var result = JsonConvert.DeserializeObject<List<Team>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/teams")).Content.ReadAsStringAsync());
            return result;
        }

        public static async Task<List<User>> GetAllUsers()
        {
            var result = JsonConvert.DeserializeObject<List<User>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/users")).Content.ReadAsStringAsync());
            return result;
        }
        // 1 querry
        public static async Task<List<TaskCountForProject>> TasksCountForProject(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<TaskCountForProject>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/1/{userId}")).Content.ReadAsStringAsync());
            return result;
            
        }
        // 2 querry
        public static async Task<List<Models.Task>>  TasksForPerformer(int userId)
        {
            var result = JsonConvert.DeserializeObject<List<Models.Task>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/2/{userId}")).Content.ReadAsStringAsync());
            return result;
        }
        // 3 querry

        public static async Task<List<Tuple<int, string>>> FinishedTasksInThisYear(int userId)
        { 
            var result = JsonConvert.DeserializeObject<List<Tuple<int,string>>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/3/{userId}")).Content.ReadAsStringAsync());
            return result;
        }
        // 4 querry

        public static async Task<List<Tuple<int, string, List<User>>>> TeamsOlder12()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<int, string, List<User>>>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/4")).Content.ReadAsStringAsync());
            return result;
        }

        // 5 querry 
        public static async Task<List<Tuple<User, List<Models.Task>>>> GetSortedUsers()
        {
            var result = JsonConvert.DeserializeObject<List<Tuple<User, List<Models.Task>>>>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/5")).Content.ReadAsStringAsync());
            return result;
        }
        // 6 querry
        public static async Task<HelperModel1> GetModel1(int userId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel1>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/6/{userId}")).Content.ReadAsStringAsync());
            return result;
        }
        // 7 querry 
        public static async Task<HelperModel2> GetModel2(int projectId)
        {
            var result = JsonConvert.DeserializeObject<HelperModel2>(await (await HelperHttpMethods.GetHttpResponse($"{URL}/queries/7/{projectId}")).Content.ReadAsStringAsync());
            return result;
        }
    }
}

