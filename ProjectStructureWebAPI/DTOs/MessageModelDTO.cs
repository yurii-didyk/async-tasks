﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructureWebAPI.DTOs
{
    public class MessageModelDTO
    {
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
