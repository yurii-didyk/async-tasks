﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;

        }
        [HttpGet]
        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            return await _teamService.Get();
        }
        [HttpGet("{id}")]
        public async Task<TeamDTO> Get([FromBody] int id)
        {
            return await _teamService.GetSingle(id);
        }
        [HttpPost]
        public async Task Add([FromBody] TeamDTO teamDto)
        {
            await _teamService.Add(teamDto);
        }
        [HttpDelete("{id}")]
        public async Task Delete([FromBody] int id)
        {
            await _teamService.Delete(id);
        }
        [HttpPut("{id}")]
        public async Task Update(int id, [FromBody] TeamDTO teamDto)
        {
            await _teamService.Update(id, teamDto);
        }

    }
}