﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;

        }
        [HttpGet]
        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            return await _userService.Get();
        }
        [HttpGet("{id}")]
        public async Task<UserDTO> Get([FromBody] int id)
        {
            return await _userService.GetSingle(id);
        }
        [HttpPost]
        public async Task Add([FromBody] UserDTO userDto)
        {
            await _userService.Add(userDto);
        }
        [HttpDelete("{id}")]
        public async Task Delete([FromBody] int id)
        {
            await _userService.Delete(id);
        }
        [HttpPut("{id}")]
        public async Task Update(int id, [FromBody] UserDTO userDto)
        {
            await _userService.Update(id, userDto);
        }

    }
}