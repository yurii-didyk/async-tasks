﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class QueriesController: ControllerBase
    {
        private readonly QueryService _queryService;

        public QueriesController(QueryService queryService)
        {
            _queryService = queryService;

        }
        [HttpGet("1/{id}")]
        public async Task<IEnumerable<TaskCountForProjectDTO>> TasksCountForProject(int id)
        {
            return await _queryService.TasksCountForProject(id);
        }
        [HttpGet("2/{id}")]
        public async Task<List<DTOs.TaskDTO>> TasksForPerformer(int id)
        {
            return await _queryService.TasksForPerformer(id);
        }
        [HttpGet("3/{id}")]
        public async Task<List<Tuple<int, string>>> FinishedTasksInThisYear(int id, int year = 2019)
        {
            return await _queryService.FinishedTasksInThisYear(id);
        }
        [HttpGet("4")]
        public async Task<List<Tuple<int, string, List<User>>>> TeamsOlder12(int age = 12)
        {
            return await _queryService.TeamsOlder12();
        }
        [HttpGet("5")]
        public async Task<List<Tuple<User, List<Models.Task>>>> GetSortedUsers()
        {
            return await _queryService.GetSortedUsers();
        }
        [HttpGet("6/{id}")]
        public async Task<HelperModel1> GetModel1(int id)
        {
            return await _queryService.GetModel1(id);
        }
        [HttpGet("7/{id}")]
        public async Task<HelperModel2> GetModel2(int id)
        {
            return await _queryService.GetModel2(id);
        }


    }
}
