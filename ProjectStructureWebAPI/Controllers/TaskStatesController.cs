﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Models;
using Newtonsoft.Json;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController: ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<object>> GetTaskStates()
        {
            return await System.Threading.Tasks.Task.Run(() =>
            {
                var items = new[] {new {id = TaskState.Started, value = Enum.GetName(typeof(TaskState), TaskState.Started) },
                                new {id = TaskState.Finished, value = Enum.GetName(typeof(TaskState), TaskState.Finished) },
                                new {id = TaskState.Canceled, value = Enum.GetName(typeof(TaskState), TaskState.Canceled) },
                                new {id = TaskState.Created, value = Enum.GetName(typeof(TaskState), TaskState.Created) } };
                return items;
            });
        }
        
    }
}
