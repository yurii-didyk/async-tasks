﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TasksController(TaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            return await _taskService.Get();
        }
        [HttpGet("{id}")]
        public async Task<TaskDTO> Get([FromBody] int id)
        {
            return await _taskService.GetSingle(id);
        }
        [HttpPost]
        public async Task Add([FromBody] TaskDTO taskDto)
        {
            await _taskService.Add(taskDto);
        }
        [HttpDelete("{id}")]
        public async Task Delete([FromBody] int id)
        {
            await _taskService.Delete(id);
        }
        [HttpPut("{id}")]
        public async Task Update(int id, [FromBody] TaskDTO taskDto)
        {
            await _taskService.Update(id, taskDto);
        }

    }
}
