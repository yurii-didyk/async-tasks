﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;

        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            return await _projectService.Get();

        }
        [HttpGet("{id}")]
        public async Task<ProjectDTO> Get(int id)
        {
            return await _projectService.GetSingle(id);
        }
        [HttpPost]
        public async Task Add([FromBody] ProjectDTO projectDto)
        {
            await _projectService.Add(projectDto);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _projectService.Delete(id);
        }
        [HttpPut("{id}")]
        public async Task Update(int id, [FromBody] ProjectDTO projectDto)
        {
            await _projectService.Update(id, projectDto);
        }

    }
}
