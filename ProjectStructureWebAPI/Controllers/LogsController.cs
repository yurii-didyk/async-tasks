﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Services;

namespace ProjectStructureWebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController: ControllerBase
    {
        private readonly LogsService _logsService;

        public LogsController(LogsService logsService)
        {
            _logsService = logsService;
        }
        [HttpGet("request")]
        public async Task SentRequest()
        {
            await _logsService.SendRequestToRecieveLogs();
        }
        [HttpPost]
        public async Task SaveLogs([FromBody] IEnumerable<MessageModelDTO> args)
        {
            await _logsService.CreateLogs(args);
        }

    }
}
