﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using ProjectStructureWebAPI.MessageSettingsModels;
using ProjectStructureWebAPI.Interfaces;

namespace ProjectStructureWebAPI.QueueServices
{
    public class MessageQueue: IMessageQueue, IDisposable
    {
        private readonly IConnection _connection;

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public IModel Channel { get; set; }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSetting)
           : this(connectionFactory)
        {
            DeclareExchange(messageScopeSetting.ExchangeName, messageScopeSetting.ExchangeType);

            if (messageScopeSetting.QueueName != null)
            {
                BindQueue(messageScopeSetting.ExchangeName, messageScopeSetting.RoutingKey, messageScopeSetting.QueueName);
            }
        }

        public void DeclareExchange(string exchangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exchangeName, exchangeType ?? string.Empty);
        }

        public void BindQueue(string exchangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exchangeName, routingKey);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }


    }
}
