﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Models
{
    public class ConnectionFactory: RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            Uri = uri;
            RequestedConnectionTimeout = 30000;
            RequestedHeartbeat = 60;
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(30);
        }
    }
}
