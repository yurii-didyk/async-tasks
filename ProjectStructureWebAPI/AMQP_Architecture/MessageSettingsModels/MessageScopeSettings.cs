﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.MessageSettingsModels
{
    public class MessageScopeSettings
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set;}
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }
    }
}
