﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Interfaces
{
    public interface IMessageConsumer
    {
        void Connect();
        void SetAcknowledge(ulong deliveryTag, bool processed);
        event EventHandler<BasicDeliverEventArgs> Received;
    }
}
