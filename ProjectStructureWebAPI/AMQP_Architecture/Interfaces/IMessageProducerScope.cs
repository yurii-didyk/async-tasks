﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Interfaces
{
    public interface IMessageProducerScope: IDisposable
    {
        IMessageProducer MessageProducer { get; }
    }
}
