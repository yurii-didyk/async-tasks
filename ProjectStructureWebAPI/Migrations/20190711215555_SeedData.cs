﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructureWebAPI.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2019, 7, 12, 0, 55, 55, 375, DateTimeKind.Local).AddTicks(9092), new DateTime(2019, 10, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Ut quisquam molestias ut.
                Quia laudantium est quo asperiores veritatis porro ut quod doloribus.
                Reiciendis odit ut hic libero.
                Fuga debitis veniam ut.", "Rerum voluptatem beatae nesciunt consectetur.", 3 },
                    { 2, 2, new DateTime(2019, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 10, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Non non aperiam aspernatur et est mollitia enim quia nihil.", "Atque quia et optio aut.", 1 },
                    { 3, 4, new DateTime(2018, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), new DateTime(2019, 11, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Cupiditate veritatis ad rerum veritatis molestiae.
                Dolorem soluta adipisci fuga iste aut est eveniet et.", "Vel aperiam qui vel animi.", 2 },
                    { 4, 3, new DateTime(2019, 7, 12, 0, 55, 55, 377, DateTimeKind.Local).AddTicks(5254), new DateTime(2019, 12, 29, 0, 0, 0, 0, DateTimeKind.Unspecified), "Debitis et ipsum esse perferendis voluptatem amet eos.", "Eius fugiat amet molestias eos.", 4 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 6, new DateTime(2019, 4, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Voluptate sunt perferendis voluptates vitae non.
                Facilis aliquam quidem aut consequatur magni facilis eaque.", new DateTime(2019, 10, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Harum quaerat impedit sed iure quia labore error rerum.", 4, 2, 0 },
                    { 4, new DateTime(2019, 4, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Nobis eius eos inventore nostrum autem eos.
                Pariatur iusto saepe et quas ratione consequuntur aut.", new DateTime(2019, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Consequatur rerum rem eos ea.", 3, 4, 0 },
                    { 5, new DateTime(2019, 6, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Corporis necessitatibus mollitia deleniti harum porro molestiae.
                Nihil est unde quidem est molestiae nisi sint.", new DateTime(2019, 11, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Voluptate distinctio id saepe reprehenderit in sint quidem.", 1, 1, 3 },
                    { 2, new DateTime(2019, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Aut eligendi aut quaerat explicabo impedit.
                Praesentium est inventore nostrum.
                Vel quasi deserunt id sapiente iste voluptas est.", new DateTime(2019, 8, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ipsam ea voluptatem optio sed doloremque.", 4, 1, 0 },
                    { 1, new DateTime(2019, 4, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Inventore quae alias magnam sed qui sint velit est.
                Suscipit dolor ut omnis voluptate.
                Exercitationem qui et temporibus dignissimos autem.", new DateTime(2019, 7, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eaque corporis illum ut.", 3, 2, 2 },
                    { 3, new DateTime(2019, 3, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), @"Et et ea voluptas laboriosam officiis ut.
                Quia deleniti itaque.
                Est molestiae magnam et nesciunt.", new DateTime(2019, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Debitis aliquid dicta maiores sed molestiae", 2, 2, 0 }
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sunt" },
                    { 2, new DateTime(2019, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Reprehenderit" },
                    { 3, new DateTime(2019, 5, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Repudiandae" },
                    { 4, new DateTime(2019, 4, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Labore" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Birthday", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 3, new DateTime(1991, 10, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "user3@gmail.com", "Anibal", "Hintz", new DateTime(2019, 4, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null },
                    { 1, new DateTime(1995, 4, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "user1@gmail.com", "Andrii", "Johnson", new DateTime(2019, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 3 },
                    { 2, new DateTime(1993, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "user2@gmail.com", "Danyka", "Gusikowski", new DateTime(2019, 5, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 4, new DateTime(1998, 8, 8, 0, 0, 0, 0, DateTimeKind.Unspecified), "user4@gmail.com", "Geovanny", "Torphy", new DateTime(2019, 6, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);
        }
    }
}
