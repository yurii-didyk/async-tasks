﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class TeamService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TeamService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(TeamDTO teamDto)
        {
            await _queueService.Post("Team creating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            await _context.Teams.AddAsync(team);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("Team deletion was triggered");
            _context.Teams.Remove(await _context.Teams.FirstOrDefaultAsync(t => t.Id == id));
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, TeamDTO teamDto)
        {
            await _queueService.Post("Team updating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            var teamToUpdate = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);

            teamToUpdate.Name = team.Name;

            _context.Teams.Update(teamToUpdate);
            await _context.SaveChangesAsync();
        }
        public async Task<TeamDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single team was triggered");
            var team = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
            return _mapper.Map<TeamDTO>(team);
        }
        public async Task<IEnumerable<TeamDTO>> Get()
        {
            await _queueService.Post("Loading all teams was triggered");
            var teams = await _context.Teams.ToListAsync();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
    }
}
