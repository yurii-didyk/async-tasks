﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class UserService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;


        public UserService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(UserDTO teamDto)
        {
            await _queueService.Post("User creation was triggered");
            var user = _mapper.Map<User>(teamDto);
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("User deletion was triggered");
            _context.Users.Remove(await _context.Users.FirstOrDefaultAsync(u => u.Id == id));
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, UserDTO teamDto)
        {
            await _queueService.Post("User updating was triggered");
            var user = _mapper.Map<User>(teamDto);
            var userToUpdate = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            userToUpdate.TeamId = user.TeamId;
            userToUpdate.Email = user.Email;
            userToUpdate.Birthday = user.Birthday;

            _context.Users.Update(userToUpdate);
            await _context.SaveChangesAsync();
        }
        public async Task<UserDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single user was triggered");
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            return _mapper.Map<UserDTO>(user);
        }
        public async Task<IEnumerable<UserDTO>> Get()
        {
            await _queueService.Post("Loading all users was triggered");
            var users = await _context.Users.ToListAsync();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
    }
}