﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.DTOs;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI.Services
{
    public class QueryService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public QueryService(
            DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }
        public async Task<IEnumerable<TaskCountForProjectDTO>> TasksCountForProject(int userId)
        {
            await _queueService.Post("TasksCountForProject query was triggered");

            var result = await (from task in _context.Tasks
                          group task by task.ProjectId into tasks
                          join project in _context.Projects on tasks.Key equals project.Id
                          where project.AuthorId == userId
                          select new TaskCountForProjectDTO
                          {
                              Project = _mapper.Map<ProjectDTO>(project),
                              TasksCount = tasks.Count()
                          }).ToListAsync();
            return result;
        }
        public async Task<List<DTOs.TaskDTO>> TasksForPerformer(int userId)
        {
            await _queueService.Post("TasksForPerformer query was triggered");
            var result = await _context.Tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToListAsync();
            return _mapper.Map<List<DTOs.TaskDTO>>(result);
        }

        public async Task<List<Tuple<int, string>>> FinishedTasksInThisYear(int userId, int year = 2019)
        {
            await _queueService.Post("FinishedTasksInThisYear query was triggered");
            var result = await _context.Tasks.Where(t => t.State == TaskState.Finished
                && t.FinishedAt.Year == year && t.PerformerId == userId)
                .Select(t => new Tuple<int, string>(t.Id, t.Name)).ToListAsync();
            return result;
        }

        public async Task<List<Tuple<int, string, List<User>>>> TeamsOlder12(int age = 12)
        {
            await _queueService.Post("TeamsOlder12 query was triggered");
            var result = await (from user in  _context.Users
                          join team in _context.Teams on user.TeamId equals team.Id
                          where 2019 - user.Birthday.Year > age
                          orderby user.RegisteredAt descending
                          group user by team into users
                          select new Tuple<int, string, List<User>>(users.Key.Id, users.Key.Name, users.ToList())).ToListAsync();
            return result;
        }

        public async Task<List<Tuple<User, List<Models.Task>>>> GetSortedUsers()
        {
            await _queueService.Post("GetSortedUsers query was triggered");
            var result = await _context.Users.OrderBy(u => u.FirstName)
                .Select(u => new Tuple<User, List<Models.Task>>(u, _context.Tasks
                                                                      .Where(t => t.PerformerId == u.Id)
                                                                      .OrderByDescending(y => y.Name.Length)
                                                                      .ToList())
                                                                ).ToListAsync();
            return result;
        }

        public async Task<HelperModel1> GetModel1(int userId)
        {
            await _queueService.Post("GetModel1 query was triggered");
            var result = await (from user in  _context.Users
                          where user.Id == userId
                          join project in  _context.Projects on user.Id equals project.AuthorId into projects
                          join task in _context.Tasks on projects.OrderBy(p => p.CreatedAt).FirstOrDefault().Id equals task.ProjectId into tasks

                          select new HelperModel1
                          {
                              User = user,
                              LastProject = projects.OrderBy(p => p.CreatedAt).FirstOrDefault(),
                              TasksCount = tasks.Count(),
                              CanceledAndUnfinishedTasksCount = _context.Tasks.Where(t => t.State != TaskState.Finished && t.PerformerId == userId).Count(),
                              LongestTask = _context.Tasks.Where(t => t.PerformerId == userId).OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
                          }).FirstOrDefaultAsync();

            return result;
        }

        public async Task<HelperModel2> GetModel2(int projectId)
        {
            await _queueService.Post("GetModel2 query was triggered");
            var result = await(from project in _context.Projects
                          where project.Id == projectId
                          join task in  _context.Tasks on project.Id equals task.ProjectId into tasks
                          select new HelperModel2
                          {
                              Project = project,
                              LongestTaskByName = tasks.OrderByDescending(t => t.Name).FirstOrDefault(),
                              LongestTaskByDescription = tasks.OrderByDescending(t => t.Description).FirstOrDefault(),
                              UsersCount = (from user in _context.Users
                                            where user.TeamId == project.TeamId && (project.Description.Length > 25 || tasks.Count() < 3)
                                            select user).Count()

                          }).FirstOrDefaultAsync();
            return result;

        }

    }
}
