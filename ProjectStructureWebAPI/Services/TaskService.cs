﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using AutoMapper;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Services
{
    public class TaskService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TaskService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async Task Add(TaskDTO taskDto)
        {
            await _queueService.Post("Task creation was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            await _context.Tasks.AddAsync(task);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(int id)
        {
            await _queueService.Post("Task deletion was triggered");
            _context.Tasks.Remove(await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id));
            await _context.SaveChangesAsync();
        }
        public async Task Update(int id, TaskDTO taskDto)
        {
            await _queueService.Post("Task updating was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            var taskToUpdate = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);

            taskToUpdate.Name = task.Name;
            taskToUpdate.Description = task.Description;
            taskToUpdate.FinishedAt = task.FinishedAt;
            taskToUpdate.PerformerId = task.PerformerId;
            taskToUpdate.ProjectId = task.ProjectId;
            taskToUpdate.State = task.State;

            _context.Tasks.Update(taskToUpdate);
            await _context.SaveChangesAsync();
        }
        public async Task<TaskDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single task was triggered");
            var task = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
            return _mapper.Map<TaskDTO>(task);
        }
        public async Task<IEnumerable<TaskDTO>> Get()
        {
            await _queueService.Post("Loading all tasks was triggered");
            var tasks = await _context.Tasks.ToListAsync();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

    }
}
