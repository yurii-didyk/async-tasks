﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class ProjectService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public ProjectService(DataContext context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(ProjectDTO projectDto)
        {
            await _queueService.Post("Project creation was triggered");
            var project = _mapper.Map<Project>(projectDto);
            await _context.Projects.AddAsync(project);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("Project deletion was triggered");
            _context.Projects.Remove(await _context.Projects.FirstOrDefaultAsync(p => p.Id == id));
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, ProjectDTO projectDto)
        {
            await _queueService.Post("Project updating was triggered");
            var project = _mapper.Map<Project>(projectDto);
            var projectToUpdate = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

            projectToUpdate.Name = project.Name;
            projectToUpdate.Description = project.Description;
            projectToUpdate.Deadline = project.Deadline;
            projectToUpdate.TeamId = project.TeamId;

            _context.Projects.Update(projectToUpdate);

            await _context.SaveChangesAsync();
        }
        public async Task<ProjectDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single project was triggered");
            var project = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
            return _mapper.Map<ProjectDTO>(project);
        }
        public async Task<IEnumerable<ProjectDTO>> Get()
        {
            await _queueService.Post("Loading all projects was triggered");
            var projects = await _context.Projects.ToListAsync();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
    }
}
