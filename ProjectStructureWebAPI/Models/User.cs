﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructureWebAPI.Models
{
    public class User
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("first_name")]
        [Required]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        [Required]
        public string LastName { get; set; }

        [JsonProperty("email")]
        [EmailAddress]
        public string Email { get; set; }

        [JsonProperty("birthday")]
        [DataType(DataType.DateTime)]
        public DateTime Birthday { get; set; }

        [JsonProperty("registered_at")]
        [DataType(DataType.DateTime)]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("team_id")]
        public int? TeamId { get; set; }

        //public Team Team { get; set; }

        //public decimal? HourlyRate { get; set; }

    }
}
