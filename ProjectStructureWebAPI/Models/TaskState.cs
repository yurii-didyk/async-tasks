﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Models
{
    [System.Serializable]
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
