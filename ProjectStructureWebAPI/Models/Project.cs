﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructureWebAPI.Models
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [MaxLength(250)]
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("created_at")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        [DataType(DataType.DateTime)]
        public DateTime Deadline { get; set; }

        [JsonProperty("author_id")]
        public int AuthorId { get; set; }

        [JsonProperty("team_id")]
        public int TeamId { get; set; }
    }
}
