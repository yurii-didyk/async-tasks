﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructureWebAPI.Models
{
    public class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        [Required]
        [MaxLength(35)]
        public string Name { get; set; }

        [JsonProperty("created_at")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        //public ICollection<User> Users { get; set; }
    }
}
