﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.Interfaces;
using ProjectStructureWebAPI.QueueServices;
using AutoMapper;
using RabbitMQ.Client;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using ProjectStructureWebAPI.Context;

namespace ProjectStructureWebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSignalR();
            services.AddDbContext<DataContext>(options => options.UseSqlServer(Configuration.GetSection("DB").Value));
            services.AddScoped<ProjectService>();
            services.AddScoped<UserService>();
            services.AddScoped<TeamService>();
            services.AddScoped<QueryService>();
            services.AddScoped<TaskService>();
            services.AddScoped<QueueService>();
            services.AddSingleton<LogsService>();
            services.AddScoped<IMessageConsumer, MessageConsumer>();
            services.AddScoped<IMessageConsumerScope, MessageConsumerScope>();
            services.AddScoped<IMessageConsumerScopeFactory, MessageConsumerScopeFactory>();
            services.AddScoped<IMessageProducer, MessageProducer>();
            services.AddScoped<IMessageProducerScope, MessageProducerScope>();
            services.AddScoped<IMessageProducerScopeFactory, MessageProducerScopeFactory>();
            services.AddScoped<IMessageQueue, MessageQueue>();
            services.AddSingleton<IConnectionFactory>(x => new Models.ConnectionFactory(new Uri(Configuration.GetSection("RabbitURL").Value)));
            var mapper = MapperConfiguration().CreateMapper();
            services.AddScoped(_ => mapper);
        }

        public MapperConfiguration MapperConfiguration()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Models.Project, DTOs.ProjectDTO>().ReverseMap();
                cfg.CreateMap<Models.Task, DTOs.TaskDTO>().ReverseMap();
                cfg.CreateMap<Models.User, DTOs.UserDTO>().ReverseMap();
                cfg.CreateMap<Models.Team, DTOs.TeamDTO>().ReverseMap();
            });
            return config;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSignalR(routes =>
            {
                routes.MapHub<Hubs.MessagesHub>("/messages");
                routes.MapHub<Hubs.LogsHub>("/logs");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
