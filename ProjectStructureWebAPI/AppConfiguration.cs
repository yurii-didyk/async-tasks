﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace ProjectStructureWebAPI
{
    public class AppConfiguration
    {
        public string AMQP { get; }
        public string Server { get; }
        public string BSA { get; }
        public string Log { get; }

        public AppConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Assembly.GetExecutingAssembly().Location + "../../../../../../ProjectStructureWebAPI", "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            AMQP = root.GetSection("RabbitURL").Value;
            Server = root.GetSection("ServerURL").Value;
            BSA = root.GetSection("BsaURL").Value;
            Log = root.GetSection("LogFileName").Value;
        }
    }
}
